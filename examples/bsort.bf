[ see https://github.com/brain-lang/brainfuck/tree/master/examples ]
[ This program sorts the bytes of its input by bubble sort. ]

>>,[>>,]<<[
[<<]>>>>[
<<[>+<<+>-]
>>[>+<<<<[->]>[<]>>-]
<<<[[-]>>[>+<-]>>[<<<+>>>-]]
>>[[<+>-]>>]<
]<<[>>+<<-]<<
]>>>>[.>>]
