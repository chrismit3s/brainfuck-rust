[ see https://github.com/brain-lang/brainfuck/tree/master/examples ]
[ Shows an ASCII representation of the Sierpinski triangle (iteration 5). ]

++++++++[>+>++++<<-]>++>>+<[-[>>+<<-]+>>]>+[
-<<<[
->[+[-]+>++>>>-<<]<[<]>>++++++[<<+++++>>-]+<<++.[-]<<
]>.>+[>>]>+
]
