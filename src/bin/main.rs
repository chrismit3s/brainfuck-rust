use brainfuckers::machine::Machine;

use std::env;
use std::error::Error;
use std::fs;
use std::io::{self, Read, Write};

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<_> = env::args().collect();
    let msg = format!("USAGE: {} {{ -p <PROGRAM> | -f <FILE> }}", args[0]);
    if args.len() != 3 {
        eprintln!("{}", msg);
        return Err(msg.into());
    }

    let program = match args[1].as_str() {
        "-p" => String::from(&args[2]),
        "-f" => fs::read_to_string(&args[2])?,
        _ => {
            eprintln!("{}", msg);
            return Err(msg.into());
        },
    };

    Machine::new(&program, &mut io::stdin().bytes().map(Result::unwrap))?
        .run()
        .for_each(|b| print!("{}", b as char));
    io::stdout().flush()?;
    Ok(())
}
