use brainfuckers::machine::Machine;

use std::env;
use std::error::Error;
use std::io::{self, Read, Write};
use std::time::{Duration, Instant};

const PROGRAM: &'static str = include_str!("../../examples/mandel.bf");
const NUM_RUNS: usize = 3;

fn format_duration(d: Duration, n: usize) -> String {
    const UNITS: &'static [(&'static str, u128)] = &[
        ("ns", 1000),
        ("us", 1000),
        ("ms", 1000),
        ("s", 60),
        ("min", 60),
        ("h", u128::MAX),
    ];

    UNITS
        .iter()
        .scan(d.as_nanos(), |remaining, (unit, size)| {
            let value = *remaining % size;
            *remaining /= size;
            Some((value, unit))
        })
        .filter(|&(v, _)| v > 0)
        .collect::<Vec<_>>()
        .into_iter()
        .rev()
        .take(n)
        .map(|(v, u)| format!("{}{}", v, u))
        .reduce(|s, vu| s + " " + &vu)
        .unwrap_or_else(|| String::from("0ns"))
}

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<_> = env::args().collect();
    let msg = format!("USAGE: {} [ <NUM_RUNS> ]", args[0]);
    if args.len() > 2 {
        eprintln!("{}", msg);
        return Err(msg.into());
    }
    let num_runs = args
        .get(1)
        .map(|s| s.parse())
        .transpose()?
        .unwrap_or(NUM_RUNS);

    let started = Instant::now();
    for _ in 0..num_runs {
        Machine::new(PROGRAM, &mut io::stdin().bytes().map(Result::unwrap))?
            .run()
            .for_each(|b| print!("{}", b as char));
    }
    io::stdout().flush()?;
    let elapsed = started.elapsed();

    let per_run = elapsed / num_runs as u32;
    println!("Per run: {}", format_duration(per_run, 3));

    Ok(())
}
