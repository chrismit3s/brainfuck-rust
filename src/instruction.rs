use std::convert;
use std::iter;

const LEFT: char = '<';
const RIGHT: char = '>';
const INC: char = '+';
const DEC: char = '-';
const READ: char = ',';
const WRITE: char = '.';
const LOOP_START: char = '[';
const LOOP_END: char = ']';

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
pub enum Instruction {
    Left(usize),
    Right(usize),
    Inc(u8),
    Dec(u8),
    Read,
    Write,
    LoopStart(usize),
    LoopEnd(usize),
}

impl Instruction {
    pub fn new(c: char) -> Option<Self> {
        match c {
            LEFT => Some(Self::Left(1)),
            RIGHT => Some(Self::Right(1)),
            INC => Some(Self::Inc(1)),
            DEC => Some(Self::Dec(1)),
            READ => Some(Self::Read),
            WRITE => Some(Self::Write),
            LOOP_START => Some(Self::LoopStart(usize::MAX)), // dummy values for jump target
            LOOP_END => Some(Self::LoopEnd(usize::MAX)),
            _ => None,
        }
    }

    pub fn merge(self, other: Self) -> (Self, Option<Self>) {
        match (self, other) {
            (Self::Left(i), Self::Left(j)) => (Self::Left(i + j), None),
            (Self::Left(i), Self::Right(j)) if i > j => (Self::Left(i - j), None),
            (Self::Left(i), Self::Right(j)) => (Self::Right(j - i), None),

            (Self::Right(i), Self::Right(j)) => (Self::Right(i + j), None),
            (Self::Right(i), Self::Left(j)) if i > j => (Self::Right(i - j), None),
            (Self::Right(i), Self::Left(j)) => (Self::Left(j - i), None),

            (Self::Inc(i), Self::Inc(j)) => (Self::Inc(i + j), None),
            (Self::Inc(i), Self::Dec(j)) if i > j => (Self::Inc(i - j), None),
            (Self::Inc(i), Self::Dec(j)) => (Self::Dec(j - i), None),
            (Self::Inc(_), Self::Read) => (Self::Read, None),

            (Self::Dec(i), Self::Dec(j)) => (Self::Dec(i + j), None),
            (Self::Dec(i), Self::Inc(j)) if i > j => (Self::Dec(i - j), None),
            (Self::Dec(i), Self::Inc(j)) => (Self::Inc(j - i), None),
            (Self::Dec(_), Self::Read) => (Self::Read, None),

            (Self::Read, Self::Read) => (Self::Read, None),

            (s, o) => (s, Some(o)),
        }
    }
}

pub fn parse(program: &str) -> Result<Vec<Instruction>, String> {
    let mut loopstack = Vec::new();
    let mut loops = Vec::new();
    let mut instructions: Vec<_> = program
        .chars()
        .filter_map(Instruction::new)
        .map(Some)
        .chain(iter::once(None))
        .scan(None::<Instruction>, |prev_op, inst_op| {
            match (*prev_op, inst_op) {
                (_, None) => Some(prev_op.take()),
                (None, Some(inst)) => {
                    *prev_op = Some(inst);
                    Some(None)
                },
                (Some(prev), Some(inst)) => {
                    let (a, b_op) = prev.merge(inst);
                    if let Some(b) = b_op {
                        *prev_op = Some(b);
                        Some(Some(a))
                    } else {
                        *prev_op = Some(a);
                        Some(None)
                    }
                },
            }
        })
        .filter_map(convert::identity)
        .enumerate()
        .map(|(i, inst)| {
            match inst {
                Instruction::LoopStart(_) => loopstack.push(i),
                Instruction::LoopEnd(_) => loops.push((
                    loopstack
                        .pop()
                        .ok_or(String::from("Too many closing brackets"))?,
                    i,
                )),
                _ => (),
            }
            Ok::<Instruction, String>(inst)
        })
        .collect::<Result<_, _>>()?;

    if loopstack.len() != 0 {
        Err(String::from("Too many opening brackets"))
    } else {
        for (start, end) in loops.into_iter() {
            instructions[start] = Instruction::LoopStart(end);
            instructions[end] = Instruction::LoopEnd(start);
        }
        Ok(instructions)
    }
}
