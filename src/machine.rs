use std::convert;
use std::iter;

use crate::instruction::{self, Instruction};
use crate::tape::Tape;

#[derive(Debug, Hash, PartialEq, Eq)]
pub struct Machine<'i, I: Iterator<Item = u8>> {
    tape: Tape,
    cursor: isize,

    program: Vec<Instruction>,
    loops: Vec<usize>,
    next: usize,

    input: &'i mut I,
}

impl<'i, I: Iterator<Item = u8> + 'i> Machine<'i, I> {
    pub fn new(program: &str, input: &'i mut I) -> Result<Self, String> {
        instruction::parse(program).map(|program| Self {
            tape: Tape::new(),
            cursor: 0,

            program,
            loops: Vec::new(),
            next: 0,

            input,
        })
    }

    fn step(&mut self) -> Option<u8> {
        let mut output = None;
        let cell = self.tape[self.cursor];
        match self.program[self.next] {
            Instruction::Left(i) => self.cursor -= i as isize,
            Instruction::Right(i) => self.cursor += i as isize,

            Instruction::Inc(i) => self.tape[self.cursor] = cell.wrapping_add(i),
            Instruction::Dec(i) => self.tape[self.cursor] = cell.wrapping_sub(i),

            Instruction::Read => self.tape[self.cursor] = self.input.next().unwrap(),
            Instruction::Write => output = Some(self.tape[self.cursor]),

            Instruction::LoopStart(target) if cell == 0 => self.next = target,
            Instruction::LoopEnd(target) if cell != 0 => self.next = target,
            _ => (),
        }
        self.next += 1;
        output
    }

    pub fn run(mut self) -> impl Iterator<Item = u8> + 'i {
        iter::from_fn(move || {
            if self.next < self.program.len() {
                Some(self.step())
            } else {
                None
            }
        })
        .filter_map(convert::identity)
    }
}
