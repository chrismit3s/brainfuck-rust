use std::ops::{Index, IndexMut};

#[derive(Clone, Hash, Debug, PartialEq, Eq)]
pub struct Tape {
    right: Vec<u8>,
    left: Vec<u8>,
}

impl Tape {
    const INIT_VALUE: u8 = 0;

    pub fn new() -> Self {
        Self {
            left: Vec::new(),
            right: Vec::new(),
        }
    }

    fn half_index(&self, i: isize) -> (Result<usize, usize>, &Vec<u8>) {
        let (i, half) = if i >= 0 {
            (i as usize, &self.right)
        } else {
            (-(i + 1) as usize, &self.left)
        };

        if i < half.len() {
            (Ok(i), half)
        } else {
            (Err(i), half)
        }
    }

    fn half_index_mut(&mut self, i: isize) -> (Result<usize, usize>, &mut Vec<u8>) {
        let (i, half) = if i >= 0 {
            (i as usize, &mut self.right)
        } else {
            (-(i + 1) as usize, &mut self.left)
        };

        if i < half.len() {
            (Ok(i), half)
        } else {
            (Err(i), half)
        }
    }

    fn ensure(&mut self, i: isize) -> (usize, &mut Vec<u8>) {
        match self.half_index_mut(i) {
            (Err(i), half) => {
                half.resize(i + 1, Self::INIT_VALUE);
                (i, half)
            },
            (Ok(i), half) => (i, half),
        }
    }
}

impl Index<isize> for Tape {
    type Output = u8;
    fn index(&self, i: isize) -> &Self::Output {
        if let (Ok(i), half) = self.half_index(i) {
            &half[i]
        } else {
            &Self::INIT_VALUE
        }
    }
}

impl IndexMut<isize> for Tape {
    fn index_mut(&mut self, i: isize) -> &mut Self::Output {
        let (i, half) = self.ensure(i);
        &mut half[i]
    }
}
